package com.neptuno.proyectoneptuno;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MenuActivity extends Activity {
    private String username,idUsuario;
    Intent intent2;
    protected void onCreate(Bundle savedInstanceState) {
        idUsuario = getIntent().getStringExtra("idUsuario");
        username = getIntent().getStringExtra("username");
        intent2=this.getIntent();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void Anuncios(View v) {

        Intent intent = new Intent(this, ListaAnunciosActivity.class);
        intent.putExtra("idUsuario",idUsuario);
        intent.putExtra("username",username);
        //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        System.gc();
        //cargarMenu();
    }

    private void cargarMenu() {
        this.finish();
        startActivity(intent2);
    }

    public void Salir(View v){
        super.onBackPressed();
        finish();
    }
}
