package com.neptuno.proyectoneptuno;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

public class ListaAnunciosActivity extends Activity implements Response.Listener<JSONObject>,Response.ErrorListener {
    TextView title,content,id,dateA;
    ProgressDialog progress;
    JsonObjectRequest request;
    RequestQueue mRequestQueue;
    private String titulo,contenido,idAnuncio,imagenA="",date,username,idUsuario;
    private ViewGroup layout;
    JSONArray json;
    int cantidadAnuncios,cuentaAnuncios=0;
    LayoutInflater inflater;
    LinearLayout ll;
    int x=0,width;
    final int NUMERO_ANUNCIOS=5;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_anuncios);
        idUsuario = getIntent().getStringExtra("idUsuario");
        username = getIntent().getStringExtra("username");

        title=findViewById(R.id.titleTextView);
        content=findViewById(R.id.contentTextView);
        progress=new ProgressDialog(this);
        mRequestQueue= Volley.newRequestQueue(this);
        layout = findViewById(R.id.listaa);
        inflater = LayoutInflater.from(this);
        cargarWebService();
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        width = metrics.widthPixels;
    }

    public void cargarAnuncios(View v) {
        x=cuentaAnuncios+NUMERO_ANUNCIOS;
        progress.show();
        try {
            ((ViewGroup)ll.getParent()).removeView(ll);
            while (cuentaAnuncios<cantidadAnuncios&&cuentaAnuncios<x){
                JSONObject jsonObject=null;
                jsonObject=json.getJSONObject(cuentaAnuncios);
                titulo=jsonObject.optString("title");
                contenido=jsonObject.optString("content");
                idAnuncio=jsonObject.optString("aID");
                imagenA=jsonObject.optString("name");
                date=jsonObject.optString("date");
                agregarAnuncio();
                cuentaAnuncios++;
            }
            progress.hide();
            if(cuentaAnuncios==x&&cantidadAnuncios>x){
                int id = R.layout.button_cargar_mas;
                ll = (LinearLayout) inflater.inflate(id, null, false);
                layout.addView(ll);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor "+e, Toast.LENGTH_LONG).show();
            progress.hide();
        }
    }

    public void abrirAnuncio(View v) {
        id=v.findViewById(R.id.idTV);
        title=v.findViewById(R.id.titleTextView);
        content=v.findViewById(R.id.contenidoCompleto);
        dateA=v.findViewById(R.id.dateTV);
        String idA,tituloA,contenidoA,fechaA;
        idA=id.getText().toString();
        tituloA=title.getText().toString();
        contenidoA=content.getText().toString();
        fechaA=dateA.getText().toString();
        Intent intent = new Intent(this, AnuncioActivity.class);
        intent.putExtra("idA", idA);
        intent.putExtra("titulo", tituloA);
        intent.putExtra("idUser",idUsuario);
        intent.putExtra("contenido",contenidoA);
        intent.putExtra("fecha",fechaA);
        intent.putExtra("username",username);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void cargarWebService() {
        progress.setMessage("Cargando...");
        progress.show();
        String url="http://cotoneptuno.x10.mx/d/lista_anuncio.php";
        request = new JsonObjectRequest(Request.Method.POST, url, null, this, this);

        // request.add(jsonObjectRequest);
        mRequestQueue.add(request);
            //VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getApplicationContext(), "Error "+error.toString(), Toast.LENGTH_LONG).show();
        progress.hide();
    }

    @Override
    public void onResponse(JSONObject response) {
        //JSONObject jsonObject=null;

        json=response.optJSONArray("announcements");

        try {
            cantidadAnuncios=json.length();
            //for (int i=0;i<json.length();i++){
            while (cuentaAnuncios<=cantidadAnuncios&&cuentaAnuncios<NUMERO_ANUNCIOS){
                JSONObject jsonObject=null;
                jsonObject=json.getJSONObject(cuentaAnuncios);
                titulo=jsonObject.optString("title");
                contenido=jsonObject.optString("content");
                idAnuncio=jsonObject.optString("aID");
                imagenA=jsonObject.optString("name");
                date=jsonObject.optString("date");
                agregarAnuncio();
                progress.hide();
                cuentaAnuncios++;
            }
            if(cuentaAnuncios==NUMERO_ANUNCIOS&&cantidadAnuncios>NUMERO_ANUNCIOS){
                int id = R.layout.button_cargar_mas;
                ll = (LinearLayout) inflater.inflate(id, null, false);
                layout.addView(ll);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                    " "+response, Toast.LENGTH_LONG).show();
            progress.hide();
        }

    }

    private void agregarAnuncio() {

        int id = R.layout.preview_anuncio;

        RelativeLayout relativeLayout = (RelativeLayout) inflater.inflate(id, null, false);

        TextView textView = relativeLayout.findViewById(R.id.titleTextView);
        textView.setText(titulo);
        TextView textViewX = relativeLayout.findViewById(R.id.contenidoCompleto);
        textViewX.setText(contenido);
        TextView textView2 = relativeLayout.findViewById(R.id.contentTextView);
        textView2.setText(contenido);
        TextView textView3 = relativeLayout.findViewById(R.id.idTV);
        textView3.setText(idAnuncio);
        TextView textView4 = relativeLayout.findViewById(R.id.dateTV);
        textView4.setText(date);
        ImageView imagen = relativeLayout.findViewById(R.id.imagenAnuncio);
        if(imagenA!="null") {
            String imgUrl = "http://cotoneptuno.x10.mx/images/"+imagenA;
            Picasso.get()
                    .load(imgUrl)
                    .resize(width, 400)
                    .centerCrop()
                    .into(imagen);
            //Picasso.get().load(imgUrl).into(imagen);
        }
        else{
            imagen.setImageResource(R.drawable.anunciosdef);
        }
        layout.addView(relativeLayout);

    }

}
