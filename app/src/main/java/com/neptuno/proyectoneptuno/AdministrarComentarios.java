package com.neptuno.proyectoneptuno;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AdministrarComentarios implements Response.ErrorListener, Response.Listener<JSONObject> {

    JsonObjectRequest request;
    RequestQueue mRequestQueue;
    JSONArray json;
    String url,idA,idUser,contenido,fecha,idComentario;
    String imageUrls="",aux="";
    Context context2;
    int codigo=0;

    public AdministrarComentarios(RequestQueue mRequestQueue,String idA,String idUser,String contenido,String fecha,Context context,int codigo,String idComentario){
        this.idA=idA;
        this.idUser=idUser;
        this.mRequestQueue=mRequestQueue;
        this.contenido=contenido;
        this.fecha=fecha;
        this.context2=context;
        this.codigo=codigo;
        this.idComentario=idComentario;
    }

    public void cargarWebServices() {
        /*String url="http://cotoneptuno.x10.mx/d/insertar_comentario.php";
        request = new JsonObjectRequest(Request.Method.POST, url, null, this, this){
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("idA", idA);
                map.put("idUSer", idUser);
                map.put("contenido", contenido);
                map.put("fecha", fecha);
                return map;
            }
        };*/
        if(codigo==1){
            url="http://cotoneptuno.x10.mx/d/insertar_comentario.php?idA="+idA+"&idUser="+idUser+"&contenido="+contenido+"&fecha="+fecha;
            url=url.replace(" ","%20");
            request = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
            mRequestQueue.add(request);
        }
        else if(codigo==2){
            url="http://cotoneptuno.x10.mx/d/eliminar_comentario.php?idA="+idA+"&idComentario="+idComentario;
            url=url.replace(" ","%20");
            request = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
            mRequestQueue.add(request);
        }
        else if(codigo==3){
            url="http://cotoneptuno.x10.mx/d/imagenes_anuncio.php?idA="+idA;
            //url=url.replace(" ","%20");
            request = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
            mRequestQueue.add(request);

        }

    }

    private void setUrl(String imageUrls2){

        this.imageUrls+=imageUrls2;
        //Toast.makeText(context2,"AQUI "+imageUrls,Toast.LENGTH_LONG).show();
    }


    @Override
    public void onErrorResponse(VolleyError volleyError) {
    }

    @Override
    public void onResponse(JSONObject response) {
        if(codigo==1){

        }
        else if(codigo==2){

        }
        else if(codigo==3){
            json=response.optJSONArray("images");
            try {
                for (int i=0;i<json.length();i++){
                    JSONObject jsonObject=null;
                    jsonObject=json.getJSONObject(i);
                    aux+=jsonObject.optString("name");

                    if(aux.equals("0")){break;}
                    if(i+1<json.length()){aux+=",";}
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            setUrl(aux);
        }

    }

}
