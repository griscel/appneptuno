package com.neptuno.proyectoneptuno;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AnuncioActivity extends Activity implements Response.ErrorListener, Response.Listener<JSONObject> {

    String idA,tituloA,idUser,idU,idComment,contenidoA,dateA,nombreUsuario,url,imageurls="";
    TextView titulo,texto;
    ProgressDialog progress;
    JsonObjectRequest request;
    RequestQueue mRequestQueue;
    JSONArray json;
    LayoutInflater inflater;
    Button imgbtn;
    Context context;
    private String fecha,contenido,username,aux="";
    private ViewGroup layout;
    int code=0,width;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anuncio);

        idA= getIntent().getExtras().getString("idA");
        tituloA = getIntent().getExtras().getString("titulo");
        idUser = getIntent().getExtras().getString("idUser");
        contenidoA = getIntent().getExtras().getString("contenido");
        dateA = getIntent().getExtras().getString("fecha");
        nombreUsuario= getIntent().getExtras().getString("username");

        layout = findViewById(R.id.comentariosA);
        context=getApplicationContext();
        progress=new ProgressDialog(this);
        mRequestQueue = Volley.newRequestQueue(this);
        inflater = LayoutInflater.from(this);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        width = metrics.widthPixels;
        code=3;
        cargarComponentes();
        code=1;
        cargarWebService();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //codigo adicional
        this.finish();
    }

    public void cargarComponentes(){
        titulo=findViewById(R.id.tituloA);
        titulo.setText(tituloA);
        texto=findViewById(R.id.texto);
        texto.setText("Publicado: "+dateA+"\n"+contenidoA);
        cargarWebService();
    }

    private void cargarWebService() {
        //mRequestQueue = new RequestQueue(new NoCache(), new BasicNetwork(new HurlStack()));
        progress.setMessage("Cargando...");
        progress.show();
            if(code==1) {
                url = "http://cotoneptuno.x10.mx/d/detalle_anuncio.php?id=" + idA;
                request = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
                //request.setShouldCache(false);
                mRequestQueue.add(request);
            }
            else if (code==3) {
                url="http://cotoneptuno.x10.mx/d/imagenes_anuncio.php?idA="+idA;
                //url=url.replace(" ","%20");
                request = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
                mRequestQueue.add(request);
            }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getApplicationContext(), "Error "+error.toString(), Toast.LENGTH_LONG).show();
        progress.hide();
    }

    @Override
    public void onResponse(JSONObject response) {
            json = response.optJSONArray("comments");
            try {
                for (int i = 0; i < json.length(); i++) {
                    JSONObject jsonObject = null;
                    jsonObject = json.getJSONObject(i);
                    idU = jsonObject.optString("id");
                    if (idU.equals("0")) {
                        break;
                    }
                    idComment = jsonObject.optString("commentID");
                    fecha = jsonObject.optString("commentsDate");
                    contenido = jsonObject.optString("commentsContent");
                    username = jsonObject.optString("username");
                    agregarComentario();
                }
                progress.hide();
            } catch (Exception e) {
                json=response.optJSONArray("images");
                try {
                    for (int i=0;i<json.length();i++) {
                        JSONObject jsonObject = null;
                        jsonObject = json.getJSONObject(i);
                        aux += jsonObject.optString("name");
                        if(aux.equals("0")){imageurls+="http://cotoneptuno.x10.mx/images/0";break;}
                        imageurls+="http://cotoneptuno.x10.mx/images/";
                        imageurls += jsonObject.optString("name");
                        if(i+1<json.length()){imageurls+=",";}
                    }
                    //Toast.makeText(getApplicationContext(),imageurls,Toast.LENGTH_LONG).show();
                    String[] parts = imageurls.split(",");
                    //Toast.makeText(getApplicationContext(),parts[0],Toast.LENGTH_LONG).show();
                    ViewPager viewPager = findViewById(R.id.viewPager);
                    ViewPagerAdapter adapter = new ViewPagerAdapter(this, parts,width);
                    viewPager.setAdapter(adapter);

                } catch (Exception f) {
                    f.printStackTrace();
                    Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                            " " + response, Toast.LENGTH_LONG).show();
                    progress.hide();
                }

            }
            /*Toast.makeText(getApplicationContext(),"AQUI 2",Toast.LENGTH_LONG).show();
            */
    }

    public void hacerComentario(View v){
        //  FALTA PONER ID AL COMENTARIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
        progress.setMessage("Cargando...");
        progress.show();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.getDefault());
        Date date = new Date();
        EditText editText = findViewById(R.id.comentarioET);
        username=nombreUsuario;
        contenido=editText.getText().toString();
        fecha=dateFormat.format(date);
        idU=idUser;
        if(!contenido.equals("")){
            editText.setText("");
            agregarComentario();
            code=1;
            AdministrarComentarios ci = new AdministrarComentarios(mRequestQueue,idA,idUser,contenido,fecha,context,code,"");
            ci.cargarWebServices();
            progress.hide();
            //LinearLayout ll = (LinearLayout) findViewById(R.id.comentariosA);
            //ll.removeAllViews();
        }
        else {
            Toast.makeText(getApplicationContext(),"Comentario vacío",Toast.LENGTH_LONG).show();
        }
        final ScrollView sv = findViewById(R.id.comentarios);
        sv.post(new Runnable() {
            public void run() {
                sv.fullScroll(sv.FOCUS_DOWN);
            }
        });
    }

    public void eliminarComentario(View v){
        LinearLayout ll = (LinearLayout) findViewById(R.id.comentariosA);
        progress.setMessage("Cargando...");
        progress.show();
        String idcmnt=v.getContentDescription().toString();
        code=2;
        AdministrarComentarios ci = new AdministrarComentarios(mRequestQueue,idA,idUser,contenido,fecha,context,code,idcmnt);
        ci.cargarWebServices();
        progress.hide();
        ll.removeView((View) v.getParent());
    }

    private void agregarComentario() {
        int id = R.layout.comentario_anuncio;

        RelativeLayout relativeLayout = (RelativeLayout) inflater.inflate(id, null, false);

        TextView textView = relativeLayout.findViewById(R.id.nombreUser);
        textView.setText(username);
        TextView textView2 = relativeLayout.findViewById(R.id.contenido);
        textView2.setText(contenido);
        TextView textView3 = relativeLayout.findViewById(R.id.fecha);
        textView3.setText(fecha);
        TextView textView4 = relativeLayout.findViewById(R.id.idCmnt);
        textView4.setText(idComment);

        if(idU.equals(idUser)){
            imgbtn = relativeLayout.findViewById(R.id.elimbtn);
            imgbtn.setContentDescription(idComment);
            imgbtn.setVisibility(View.VISIBLE);
        }

        else{
            imgbtn = relativeLayout.findViewById(R.id.elimbtn);
            imgbtn.setVisibility(View.GONE);
        }
        layout.addView(relativeLayout);
    }

}
